FROM registry.access.redhat.com/redhat-openjdk-18/openjdk18-openshift:1.4

COPY /var/jenkins_home/workspace/test-pipeline/spring-petclinic-microservices/spring-petclinic-admin-server/target/spring-petclinic-admin-server-2.1.4.jar /deployments

RUN chmod 777 /deployments/spring-petclinic-admin-server-2.1.4.jar
